'use strict';

/* READY */

(function () {

  $(document).ready(function () {

// PAUSE video on swipe Slider
	  $('#production__video-slide-nav').on('swipe', function(event, slick, direction){
		  pauseVideo($('.js-videoTab'));
		  $(".js-play-video").removeClass('video-previews-hide');
	  });

//SLIDES
    $(function(){
	    $('#production__video-slide').slick({
		    slidesToShow: 1,
		    slidesToScroll: 1,
		    arrows: false,
		    fade: true,
		    swipe: false,
		    touchMove: false,
		    swipeToSlide: false,
		    asNavFor: '#production__video-slide-nav'
	    });
	    $('#production__video-slide-nav').slick({
		    loop: false,
		    slidesToShow: 3,
		    slidesToScroll: 1,
		    asNavFor: '#production__video-slide',
		    dots: false,
		    arrows: false,
		    centerMode: false,
		    centerPadding: 0,
		    vertical: true,
		    focusOnSelect: true,
		    responsive: [
			    {
				    breakpoint: 767,
				    settings: {
					    vertical: false
				    }
			    },
			    {
				    breakpoint: 575,
				    settings: {
					    vertical: false,
					    slidesToShow: 2
				    }
			    }
		    ],
	    });
});

    // Change language
    $('.js-lang').click(function () {
      $(this).addClass('header__lang-item_chosen').siblings().removeClass('header__lang-item_chosen');
    });

    // Submenu
    $('.main-nav__submenu').css('top', $('.main-nav').height());

    // Main Slider
    $('.carousel').carousel();

    // More Products
    $('.js-more-btn').click(function (e) {
      e.preventDefault();
      $(this).add('.more-products__wrapper').toggleClass('open');
    });

    // Custom Scrollbar
    $('.scroll_x').mCustomScrollbar({
      theme: "dark",
      axis:"x"
    });

    // Modal Callback
    $('.js-callback').on('click', function (e) {
      e.preventDefault();
      $('.modal-callback').addClass('open');
    });

    $('.js-callback-close').on('click', function (e) {
      $('.modal-callback').removeClass('open');
    });

    $('.modal-callback').on('click', function (e) {
      if ($(event.target).closest(".modal-callback__wrapper").length || $(event.target).closest(".js-callback").length ) return;
      $(this).removeClass('open');
    });

  });

	// anchor
	$('.anchor').click(function () {
		var scroll_el = $(this).attr('href');
		if ($(scroll_el).length != 0) {
			$('html, body').animate({scrollTop: $(scroll_el).offset().top - 50}, 1000);
		}
		return false;
	});

  // $(window).on('resize', function() {
  //   // Here we write code on resize ...
  // });

})();

// VIDEO click (JavaScript)
function pauseVideo(video){
	var _videoAll = video;
	for( var i = 0; i < _videoAll.length; i++ ){
		_videoAll[i].pause();
	}
}

(function(){
	var _video = document.querySelectorAll(".js-videoTab"),
		js_poster = document.querySelectorAll(".js-play-video"),
		js_pause = document.querySelectorAll(".js-pause-video");

	for( var i = 0; i < js_pause.length; i++ ){
		js_pause[i].addEventListener('click', function(){
			pauseVideo(_video);
			for( var i = 0; i < js_poster.length; i++ ){
				js_poster[i].classList.remove('video-previews-hide');
			}
		}, false);
	}

	for( var i = 0; i < js_poster.length; i++ ){
		js_poster[i].addEventListener('click', function(){
			var _play = this.nextElementSibling;
				_play.play();
				this.classList.add('video-previews-hide');
		}, false);
	}
}());
