// 'use strict';

var gulp         = require('gulp'),
    sass         = require('gulp-sass'),
    browserSync  = require('browser-sync'),
    concat       = require('gulp-concat'),
    uglify       = require('gulp-uglifyjs'),
    cssnano      = require('gulp-cssnano'),
    rename       = require('gulp-rename'),
    del          = require('del'),
    imagemin     = require('gulp-imagemin'),
    pngquant     = require('imagemin-pngquant'),
    cache        = require('gulp-cache'),
    autoprefixer = require('gulp-autoprefixer'),
    gcmq         = require('gulp-group-css-media-queries'),
    babel        = require('gulp-babel'),
    pug          = require('gulp-pug'),
    sourcemaps   = require('gulp-sourcemaps'),
    svgSprite    = require('gulp-svg-sprites'),
    notify       = require("gulp-notify");;

var path = {
  src: {
  	html: './src/',
  	js: './src/js/**/*.js',
  	styles: './src/css/',
  	sass: './src/sass/**/*.scss',
  	css: './src/css/**/*.css',
  	img: './src/img/**/*',
  	svg: './src/svg/**/*.svg',
    svgOut: './src/svg/',
  	fonts: './src/fonts/**/*',
    module: './src/module/**/*',
    favicon: './src/favicon/**/*',
    pug: './src/pug/pages/*.pug'
  },
  dist: {
  	html: './dist/',
  	js: './dist/js/',
  	css: './dist/css/',
  	img: './dist/img/',
  	svg: './dist/svg/',
  	fonts: './dist/fonts/',
    module: './dist/module/',
    favicon: './dist/favicon/'
  }
};

// Task for reloading browser
gulp.task('browser-sync', function () {
  browserSync({
    server: {
      baseDir: path.src.html
    },
    notify: false
  });
});

gulp.task('sass', function () {
  return gulp.src(path.src.sass)
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .on('error', notify.onError(function (err) {
      return {
        title: 'STYLES ERROR!',
        message: err.message
      }
    }))
    .pipe(autoprefixer(['last 15 versions', '>1%', 'ie 8', 'ie 7'], { cascade: true}))
    .pipe(gcmq())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.src.styles))
    .pipe(browserSync.reload({stream: true}))
});

gulp.task('scripts', function () {
  return gulp.src(path.src.js)
  .pipe(uglify())
  .pipe(gulp.dest(path.dist.js))
});

// Task minifies css (For production)
gulp.task('css-libs', ['sass'], function () {
  return gulp.src(path.src.css)
  .pipe(cssnano())
  // .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest(path.src.styles));
});

gulp.task('clean', function () {
  return del.sync('dist');
});

// Task for clear cashe
gulp.task('clear', function () {
  return cache.clearAll();
});

// Task make images minified
gulp.task('img', function () {
  return gulp.src(path.src.img)
  .pipe(cache(imagemin({
    interlaced: true,
    progressive: true,
    svgoPlugins: [{removeViewBox: false}],
    une: [pngquant()]
  })))
  .pipe(gulp.dest(path.dist.img));
});

// gulp.task('svg', function () {
//   return gulp.src(path.src.svgOut + '/icons/**/*')
//     .pipe(svgSprite({mode: "symbols"}))
//     .pipe(gulp.dest(path.src.svgOut))
//     .pipe(browserSync.reload({
//       stream: true
//     }));
// });

// Babel
// gulp.task('es6', function () {
//   return gulp.src('src/js/es6/**/*.js')
//   .pipe(babel({
//     presets: ['es2015']
//   }))
//   .pipe(gulp.dest('src/js'));
// });

// Pug
gulp.task('pug', function () {
  return gulp.src(path.src.pug)
  .pipe(pug({
    pretty: true
  }))
  .on('error', notify.onError(function (err) {
    return {
      title: 'PUG ERROR!',
      message: err.message
    }
  }))
  .pipe(gulp.dest(path.src.html))
  .pipe(browserSync.reload({
    stream: true
  }));
});

// If need watch pug or es6 add in [] - 'pug', 'es6', 'scripts', 'css-libs'
gulp.task('watch', ['browser-sync', 'pug'], function () {
  gulp.watch(path.src.sass, ['sass']);
  gulp.watch(path.src.pug, ['pug']);
  gulp.watch(path.src.html + '*.html', browserSync.reload);
  gulp.watch(path.src.js, browserSync.reload)
});

gulp.task('build', ['clean', 'img', 'sass', 'scripts', 'css-libs'], function () {
   gulp.src(path.src.css)
    .pipe(gulp.dest(path.dist.css));

   gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.dist.fonts));

   gulp.src(path.src.html + '*.html')
    .pipe(gulp.dest(path.dist.html));

   gulp.src(path.src.favicon)
    .pipe(gulp.dest(path.dist.favicon));

   gulp.src(path.src.module)
    .pipe(gulp.dest(path.dist.module));

   gulp.src(path.src.svg)
    .pipe(gulp.dest(path.dist.svg));


});

gulp.task('default', ['watch']);
